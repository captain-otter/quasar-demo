const express = require("express");
const app = express();
const dotenv = require("dotenv");
const cors = require("cors");
let port = 3000;

app.use(cors());

if (process.env.NODE_ENV !== "production") {
  dotenv.config({ path: "../.env.dev" });
  console.log("Using DEV .env");
}

app.get("/", function (req, res) {
  res.send("Hello World");
});

app.get("/debug", function (req, res) {
  res.send(process.env.DEBUG_VAR);
});

//Listen on the defined port
app.listen(port, function () {
  console.log(`Listening on port ${port}`);
});
