## The issue

Quasar framework can't access the environment variables defined in the docker-compose.yml

## Project structure

```
root
├── .env
├── .env.dev
├── docker-compose.yml
├── client
│   └── quasar files
└── server
    └── express.js files

```

## Workflow

### Developing

I run `npm run dev` to start the express backend and `quasar dev` to start the quasar frontend. Both frond- and backend use the same `.env.dev` file that is located in the root.

#### Express accesses it like this:

```
const  express = require("express");
const  app = express();
const  dotenv = require("dotenv");

if (process.env.NODE_ENV !== "production") {
	dotenv.config({ path:  "../.env.dev" });
}
```

#### Quasar accesses it using dotenv extension like this:

```
{
	"@quasar/dotenv": {
	"env_development": "../.env.dev",
	"env_production": "../.env",
	"common_root_object": "none",
	"create_env_files": false,
	"add_env_to_gitignore": false
	}
}
```

### Building the Quasar locally

If quasar frontend is built locally using `quasar build` and then served using `quasar serve`, it can access the environment variables. This works because `.env` file is still present at the root level.

### Deploying to production (Using Docker)

Deploying to production is done using docker. Docker compose file makes sure to pass the environment variables from the root `.env` file down to the containers. So neither frontend or backend have access to the `.env` file, because it is not copied into the containers.

## Conclusion

**Express:** dotenv package seems to be able to also read the environment variables from the container.
**Quasar:** dotenv extension seems to rely on existence of the .env file in the root of the project.
